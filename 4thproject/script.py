from passlib.hash import bcrypt

hashed = "$2b$12$oCDcRWQzuugJjS7o/h6gpOo/rbxFl9l5nF33JZQuyPKpvo.h1lk3q"
password = "2dv"

print(bcrypt.verify(password,hashed))