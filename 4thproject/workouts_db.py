import sqlite3

def dict_factory(cursor, row):
    d = {}
    for idx, col in enumerate(cursor.description):
        d[col[0]] = row[idx]
    return d

class WorkoutsDB:
    def __init__(self):
        self.connection = sqlite3.connect("mydb.db")  #connects us to our db
        self.connection.row_factory = dict_factory
        self.cursor = self.connection.cursor()   #lets us loop over data and get it back

    def insertUser(self, fname, lname, email, password):
        print("INSERTING USER")
        data = [fname, lname, email, password]
        self.cursor.execute("INSERT INTO users (fname, lname, email, password) Values (?,?,?,?)", data)
        
        self.connection.commit()


    def insertWorkout(self, name, sets, reps, tut, rest):
        data = [name, sets, reps, tut, rest]
        self.cursor.execute("INSERT INTO workouts (name, sets, reps, tut, rest) VALUES (?,?,?,?,?)", data)
        self.connection.commit()

    def getWorkouts(self):
        self.cursor.execute("SELECT * FROM workouts")
        result = self.cursor.fetchall() #return as list of dictionaries it is a list of tuples now (row_factory python sqlite3)
        return result
    
    def getOneWorkout(self, workout_id):
        data = [workout_id]
        self.cursor.execute("SELECT * FROM workouts WHERE id = ?", data)
        result = self.cursor.fetchone()
        return result

    def deleteOneWorkout(self, workout_id):
        data = [workout_id]
        print("data", data)

        self.cursor.execute("DELETE FROM workouts WHERE id = ?", data)
        self.connection.commit()

    def updateOneWorkout(self, workout_id, name, sets, reps, tut, rest):
        
        data = [name, sets, reps, tut, rest, workout_id]
        self.cursor.execute("UPDATE workouts SET name = ?, sets = ?, reps = ?, tut = ?, rest = ? WHERE id = ?", data)
        self.connection.commit()
    
    def emailExists(self, email):
        data = [email]
        self.cursor.execute("SELECT email FROM users WHERE email = ?", data)
        return self.cursor.fetchone()
    
    def getPassword(self, email):
        data = [email]
        self.cursor.execute("SELECT password FROM users WHERE email = ?", data)
        result = self.cursor.fetchone()
        return result

        
    
    

#print(result[0][0]) #print first item out of first tuple
#print(cursor.fetchall()) #prints out list of tuples











