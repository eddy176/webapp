from http.server import BaseHTTPRequestHandler, HTTPServer
from urllib.parse import parse_qs
from workouts_db import WorkoutsDB
import json
from passlib.hash import bcrypt


class RequestHandler(BaseHTTPRequestHandler): #include base class BashHTTP to my class

    def do_OPTIONS(self):
        self.send_response(200)
        self.send_header("Access-Control-Allow-Origin", "*")
        self.send_header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT, UPDATE, OPTIONS")
        self.send_header("Access-Allow-Control-Headers",  "Content-Type")
        self.end_headers()

    def do_GET(self):
        print("Path is", self.path)
        if self.path == "/workouts":
            self.handleWorkoutsRetrieveCollection()
        elif self.path.startswith("/workouts/"):
            self.handleWorkoutsRetrieveMember()
        else:
            self.handleNotFound()

    def do_DELETE(self):
        if self.path.startswith("/workouts/"):
            self.handleWorkoutsDeletemember()
        else:
            self.handleNotFound()

    def do_PUT(self):
        if self.path.startswith("/workouts/"):
            self.handleWorkoutsUpdateMember()
        else:
            self.handleNotFound

    def do_POST(self):
        print("Path is", self.path)
        if self.path == "/workouts":
            self.handleWorkoutsCreate()
        elif self.path == "/users":
            self.handleUserCreate()
        elif self.path.startswith("/users/"):
            self.handleUserSignIn()
        else:
            self.handleNotFound()

    def handleNotFound(self):
        self.send_response(404)
        self.end_headers()
        self.wfile.write(bytes("Not found.", "utf-8"))
    
    def handleWorkoutsRetrieveCollection(self):
        #respon accordingly
        self.send_response(200)
        self.send_header("Content-Type", "application/json")
        self.send_header("Access-Control-Allow-Origin", "*")
        self.end_headers()
        #send body
        db = WorkoutsDB()
        workouts = db.getWorkouts()
        self.wfile.write(bytes(json.dumps(workouts), "utf-8"))
    
    def handleWorkoutsRetrieveMember(self):
        parts = self.path.split("/")
        workout_id = parts[2]
        db = WorkoutsDB()
        workout = db.getOneWorkout(workout_id)
        print("workout", workout)
        if workout != None:
            self.send_response(200)
            self.send_header("Content-Type", "application/json")
            self.send_header("Access-Control-Allow-Origin", "*")
            self.end_headers()
            self.wfile.write(bytes(json.dumps(workout), "utf-8"))
        else: 
            self.handleNotFound()
    
    def handleWorkoutsDeletemember(self):
        parts = self.path.split("/")
        workout_id = parts[2]
        print("id", workout_id)
        
        db = WorkoutsDB()
        exists = db.getOneWorkout(workout_id)
        if exists != None:
            db.deleteOneWorkout(workout_id)

            self.send_response(200)
            self.send_header("Content-Type", "application/json")
            self.send_header("Access-Control-Allow-Origin", "*")
            self.end_headers()
        else:
            self.handleNotFound()


    def handleWorkoutsCreate(self):
        #read the body 
        length = self.headers["Content-Length"]
        body = self.rfile.read(int(length)).decode("utf-8")
        print("BODY: ", body)

        #parse body into dictionary using parse_qs()
        parsed_body = parse_qs(body)
        name = parsed_body["name"][0]
        sets = parsed_body["sets"][0]
        reps = parsed_body["reps"][0]
        tut = parsed_body["tut"][0]
        rest = parsed_body["rest"][0]

        db = WorkoutsDB()
        db.insertWorkout(name, sets, reps, tut, rest)
       
        #respond to the client
        self.send_response(201)
        self.send_header("Access-Control-Allow-Origin", "*")
        self.end_headers()
    
    def handleWorkoutsUpdateMember(self):
        parts = self.path.split("/")
        workout_id = parts[2] 
        print ("parts", parts)
       
        print("workout id", workout_id)
        length = self.headers["Content-Length"]
        print("length: ", length)
        body = self.rfile.read(int(length)).decode("utf-8")
        print("BODY: ", body)

        #parse body into dictionary using parse_qs()
        parsed_body = parse_qs(body)
        print("parsed body", parsed_body)
        name = parsed_body["name"][0]
        sets = parsed_body["sets"][0]
        reps = parsed_body["reps"][0]
        tut = parsed_body["tut"][0]
        rest = parsed_body["rest"][0]

        print("id", workout_id)
        db = WorkoutsDB()
 
        exists = db.getOneWorkout(workout_id)
        if exists != None:
            db.updateOneWorkout(workout_id, name, sets, reps, tut, rest)

            self.send_response(200)
            self.send_header("Content-Type", "application/json")
            self.send_header("Access-Control-Allow-Origin", "*")
            self.end_headers()
        else:
            self.handleNotFound()

    def handleUserCreate(self):
        #read the body 
        length = self.headers["Content-Length"]
        body = self.rfile.read(int(length)).decode("utf-8")
        print("BODY: ", body)

        #parse body into dictionary using parse_qs()
        parsed_body = parse_qs(body)
        fname = parsed_body["fname"][0]
        lname = parsed_body["lname"][0]
        email = parsed_body["email"][0]
        password = parsed_body["password"][0]

        
        h = bcrypt.hash(password)
        print(type(h))

        db = WorkoutsDB()
        exists = db.emailExists(email)
        print("exists", exists)
        if exists == None:
            db = WorkoutsDB()
            print("fname", fname)
            print("lname", lname)
            print("email", email)
            print("password", password)
            db.insertUser(fname, lname, email, h)
        
            #respond to the client
            self.send_response(201)
            self.send_header("Access-Control-Allow-Origin", "*")
            self.end_headers()

        else:
            self.send_response(400)
            self.send_header("Access-Control-Allow-Origin", "*")
            self.end_headers()

    def handleUserSignIn(self):
        #read the body 
        length = self.headers["Content-Length"]
        body = self.rfile.read(int(length)).decode("utf-8")
        print("BODY: ", body)

        #parse body into dictionary using parse_qs()
        parsed_body = parse_qs(body)
        email = parsed_body["email"][0]
        password = parsed_body["password"][0]
        
        db = WorkoutsDB()
        exists = db.emailExists(email)
        hashed = db.getPassword(email)
        print("hashed", hashed)
        if exists != None:
            valid = bcrypt.verify(password, hashed["password"])
            if valid:
                self.send_response(200)
                self.send_header("Access-Control-Allow-Origin", "*")
                self.end_headers()
            else:
                self.send_response(401)
                self.send_header("Access-Control-Allow-Origin", "*")
                self.end_headers()
        else:
            self.send_response(401)
            self.end_headers()

        

            

        

        
def run():
    listen = ("127.0.0.1", 8080) #binding (what ip and port we are listening too)
    server= HTTPServer(listen, RequestHandler)
    
    print("listening . . .")
    server.serve_forever()
run()




